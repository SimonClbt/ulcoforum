-- create database:
-- sqlite3 ulco.db < ulco.sql

CREATE TABLE sujet (
  sujet_id INTEGER PRIMARY KEY AUTOINCREMENT,
  sujet_title TEXT
);

CREATE TABLE contenu (
  contenu_id INTEGER PRIMARY KEY AUTOINCREMENT,
  contenu_sujet INTEGER,
  contenu_user TEXT,
  contenu_message TEXT,
  FOREIGN KEY(contenu_sujet) REFERENCES sujet(sujet_id)
);

INSERT INTO sujet VALUES(1, 'FOSDEM : Le Tavernier');
INSERT INTO sujet VALUES(2, 'COVID19 : la tele-fac');
INSERT INTO sujet VALUES(3, 'Réaliser un bon poster');

INSERT INTO contenu VALUES(1, 1, 'toto', 'vivement le prochain');
INSERT INTO contenu VALUES(2, 1, 'tutu', 'Vive le logiciel libre');
INSERT INTO contenu VALUES(3, 2, 'titi', 'très bien');
INSERT INTO contenu VALUES(4, 3, 'toto', 'Ne pas mettre trop de texte');