{-# LANGUAGE OverloadedStrings #-}

module Forum where

import Data.Text (Text)
import Database.SQLite.Simple

dbSelectAll :: Connection -> IO [(Text, Text, Text)]
dbSelectAll conn = query_ conn "SELECT sujet_title, contenu_user, contenu_message from sujet INNER JOIN contenu ON sujet_id = contenu_sujet"

dbSelectAllThreads :: Connection -> IO [(Int, Text)]
dbSelectAllThreads conn = query_ conn "SELECT * FROM sujet"

dbSelectThreadById :: Int-> Connection -> IO [(Text, Text)]
dbSelectThreadById id conn = query conn "SELECT contenu_user, contenu_message FROM contenu WHERE contenu_id = (?)" (Only id)