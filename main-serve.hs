{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.IO.Class
import Database.SQLite.Simple
import Lucid
import Web.Scotty
import TextShow (showt)
import Data.Text

import Forum

main :: IO ()
main = scotty 3000 $ do
    get "/" $ do
        html $ renderText $ do
            myHeader
            p_ "C'est l'accueil"
    get "/allthreads" $ do
        res <- 
            liftIO $ withConnection "ulco.db" dbSelectAllThreads
        html $ renderText $ do
            myHeader
            ul_ $ mapM_ mkSujets res
    get "/alldata" $ do
        res <- 
            liftIO $ withConnection "ulco.db" dbSelectAll
        html $ renderText $ do
            myHeader
            ul_ $ mapM_ mkDatas res
    get "/onethread/:id" $ do
        id <- param "id"
        res <- 
            liftIO $ withConnection "ulco.db" (dbSelectThreadById id)
        html $ renderText $ do
            myHeader
            ul_ $ mapM_ idDatas res

mkDatas :: (Text, Text, Text) -> Html ()
mkDatas (title, user, contenu) = 
    li_ $ do
            b_ $ toHtml title
            p_ " "
            toHtml user
            span_ " : "
            toHtml contenu
            p_ " "

mkSujets :: (Int, Text) -> Html ()
mkSujets (id, title) = 
    li_ $ do
           a_ [href_ ("/onethread/"<>showt id)] $ toHtml title

myHeader :: Html ()
myHeader = do 
    h1_ $ a_ [href_ "/"] "ulcoform"
    a_ [href_ "/alldata"] "allData"
    span_ " - "
    a_ [href_ "/allthreads"] "allThreads"

idDatas :: (Text, Text) -> Html ()
idDatas (user, contenu) = 
    li_ $ do
            toHtml user
            span_ " : "
            p_ " "
            toHtml contenu
            


